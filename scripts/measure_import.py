#!/usr/bin/env python

from requests import post, get
from time import sleep
from datetime import datetime
from string import zfill
from json import load
from requests.exceptions import ConnectionError
from jobs_creator import createImportJob
from jobs_parser import createJobStatistic, parseJobs, \
    getImportJobsText, areAllJobsDone

CREATE_JOB_LINK = '-createJobLink'
JOB_DATA = '-jobData'
VIEW_JOB_LINK = '-viewJobsLink'
JOB_COUNT = '-jobsCount'
TIMEOUT = '-timeout'
DEFAULT_JOB_COUNT = 1
DEFAULT_TIMEOUT = 60

AVERAGE = 'average'
DONE = 'done'
VALUE = 'value'
TIME = 'time'
MIN = 'min'
MAX = 'max'
JOB = 'job'
JOBS_LIST = {
    'average': {
        'value': {}}, 'min': {
            'value': {}, 'job': {}}, 'max': {
                'value': {}, 'job': {}}}

def createImportJob(createJobLink, jobData):
    return post(createJobLink, data=jobData)


def timeConvert(data):
    return zfill(unicode(data / 3600000000), 1) + \
        ':' + zfill(unicode(data / 60000000), 2) + \
        ':' + zfill(unicode(data / 1000000), 2) + \
        '.' + unicode(data)[:6]


def getImportJobsText(viewJobsLink):
    return get(viewJobsLink).text


def parseJobs(jobsText):
    return loads(jobsText)


def areAllJobsDone(jobsList):
    for job in jobsList:
        if job.get(DONE) == False:
            return False
    return True


def createJobStatistic(jobsList):
    summ = 0
    for i in range(len(jobsList)):
        jobTime = jobsList[i].get(TIME)
        timeObj = datetime.strptime(jobTime, "%H:%M:%S.%f")
        summ += int(timeObj.strftime("%f")) + \
            (int(timeObj.strftime("%S")) + int(
                timeObj.strftime("%M")) * 60 +
             int(timeObj.strftime("%H")) * 3600) * \
            1000000
        if i == 0:
            minValue = timeObj
            maxValue = timeObj
        if timeObj > maxValue:
            maxValue = timeObj
        if minValue > timeObj:
            minValue = timeObj
    JOBS_LIST[AVERAGE][VALUE] = timeConvert(summ / len(jobsList))
    JOBS_LIST[MIN][VALUE] = minValue.strftime("%H:%M:%S.%f")
    JOBS_LIST[MIN][JOB] = jobsList
    JOBS_LIST[MAX][VALUE] = maxValue.strftime("%H:%M:%S.%f")
    JOBS_LIST[MAX][JOB] = jobsList
    return JOBS_LIST

def main(createJobLink, jobData, viewJobsLink, jobsCount, timeout):
    try:
        for _ in range(0, jobsCount):
           print createImportJob(createJobLink, jobData)
    except ConnectionError:
        print "No connection to " + createJobLink 
        return 1
    sleep(timeout)
    jobsText = getImportJobsText(viewJobsLink)
    jobsList = parseJobs(jobsText)
    if not areAllJobsDone(jobsList) or len(jobsList) == 0:
        print "No results by timeout"
        return 1
    print createJobStatistic(jobsList)
    return 0

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(CREATE_JOB_LINK, type=unicode, required=True)
    parser.add_argument(JOB_DATA, type=unicode, required=True)
    parser.add_argument(VIEW_JOB_LINK, type=unicode, required=True)
    parser.add_argument(JOB_COUNT, type=int, default=DEFAULT_JOB_COUNT)
    parser.add_argument(TIMEOUT, type=int, default=DEFAULT_TIMEOUT)
    args = parser.parse_args()
    main(
        args.createJobLink,
        args.jobData,
        args.viewJobsLink,
        args.jobsCount,
        args.timeout)
