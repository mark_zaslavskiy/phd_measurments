#!/bin/bash

# args: points, days_from, days_to, method

points=$1
df=$2
dt=$3
method=$4
arr=()
arr2=()

dir_="/var/www/geomongo/"

for i in `seq ${df} ${dt}`;
  do
    echo "Params, $i ${points}"
    start=$(date +%s.%N)

    curl -d '
{
  \"channelName\" : \"testchannel\", 
  \"days_to_load\" : \""$i"\", 
  \"points_to_load\" : \""${points}"\",
  \"method\" : \""${method}"\"
}' http://geomongo/instance/plugin/ok_import/service/testservice/job

    while ! test -f "${dir_}file_d"$i'_p'${points}'.txt'; do sleep 0.01;  done

    end=$(date +%s.%N)
    diff=$(echo "${end} - ${start}" | bc)
    echo "Total Time ${diff}"
    arr[i]=${diff}
  done

for i in "${!arr[@]}"; do
  printf "%s\t%s\n" "$i" "${arr[$i]}" >> results_full.txt
done

filebase="${dir_}file"

for i in `seq ${df} ${dt}`;
  do
    read SHIT VAR < ${filebase}'_d'$i'_p'${points}'.txt'
    arr2[i]=$VAR
  done

for i in "${!arr2[@]}"; do
  printf "%s\t%s\n" "$i" "${arr2[$i]}" >> results_download.txt
done

