#!/bin/bash

days=$1
PF=$2
PT=$3
method=$4
arr=()
arr2=()

for i in `seq ${pf} ${pt}`;
  do
    printf '\nRunning plugin with params: '$i' points, '${days}' days...\n'

    start=$(date +%s.%N)

    curl -d "{
  \"channelName\":\"testchannel\",
  \"days_to_load\":\""${days}"\",
  \"points_to_load\":\""$i"\",
  \"method\":\""${method}"\"
}" http://geomongo/instance/plugin/ok_import/service/testservice/job

    while ! test -f '/var/www/geomongo/file_d'${days}'_p'$i'.txt'; do sleep 0.01; done

    end=$(date +%s.%N)
    diff=$(echo "${end} - ${start}" | bc)
    printf 'Script worked '${diff}' seconds\n'
    arr[i]=${diff}
  done 

for i in "${!arr[@]}"; do
  printf "%s\t%s\n" "$i" "${arr[$i]}" >> results_full.txt
done

filebase='/var/www/geomongo/file'

for i in `seq ${pf} ${pt}`;
  do
    read SHIT VAR < ${filebase}'_d'${days}'_p'$i'.txt'
    arr2[i]=$VAR
  done

for i in "${!arr2[@]}"; do
  printf "%s\t%s\n" "$i" "${arr2[$i]}" >> results_download.txt
done

